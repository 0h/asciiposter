# ASCII poster

This library provides a means to generate a matrix of characters coloured to resemble a chosen image onto a printable format.

The `testPoster` method in `io.zhx.asciiposter.board.PDFPosterTest` shows how to set up and output a PDF poster.
