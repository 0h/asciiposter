package io.zhx.asciiposter;

/**
 * A board is a square matrix.
 */
public interface Board {
    /**
     * @return The size of the board expressed as the order of the matrix,
     * .i.e. the number of rows or columns.
     */
    int getSize();

    /**
     * Get an element in the board.
     *
     * @param x the column
     * @param y the row
     * @return the element at the specified row and column.
     */
    byte getItem(int x, int y);
}
