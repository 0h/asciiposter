package io.zhx.asciiposter;

import io.zhx.asciiposter.board.BoardContentsProvider;

import java.util.logging.Logger;

/**
 * A board.
 */
public class BoardImpl implements Board {
    private static final Logger logger = Logger.getLogger(BoardImpl
            .class.getName());

    protected final byte[][] contents;

    /**
     * Construct a board.
     *
     * @param contentsProvider the provider that fills up the board's cells.
     */
    public BoardImpl(BoardContentsProvider contentsProvider) {
        // Fill up the board if a provider has been supplied.
        contents = contentsProvider.getContents();

        logger.info(String.format("%1$s×%1$s board created.", getSize()));
    }

    public int getSize() {
        return contents.length;
    }

    @Override
    public byte getItem(int x, int y) {
        return contents[x][y];
    }
}
