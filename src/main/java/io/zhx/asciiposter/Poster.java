package io.zhx.asciiposter;

import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * A poster.
 * <p>
 * `float`s are used instead of doubles to avoid constant conversions to suit
 * PDFBox's idiosyncrasies.
 * <p>
 * All members holding a value in millimeters are suffixed with `MM`, and
 * should be converted to points as soon as possible.
 */
public class Poster {
    private final Size size;
    private final double margin;
    private final Board board;
    private BufferedImage image;

    /**
     * @param board  the board
     * @param size   the size of the poster
     * @param margin the margin in millimeters
     */
    public Poster(Board board, Size size, double margin) {
        Objects.requireNonNull(board, "A board is required");
        this.board = board;

        Objects.requireNonNull(size, "A size is required");
        this.size = size;

        // Validate margin.
        if ((margin * 2) > this.size.getHeight()) {
            throw new IllegalArgumentException(
                    "The margin leaves no vertical printable area."
            );
        }
        if ((margin * 2) > this.size.getWidth()) {
            throw new IllegalArgumentException(
                    "The margin leaves no horizontal printable area."
            );
        }

        this.margin = margin;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public double getMargin() {
        return margin;
    }

    public Size getSize() { return size; }

    public BufferedImage getImage() {
        return image;
    }

    public Board getBoard() {
        return board;
    }
}
