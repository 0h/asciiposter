package io.zhx.asciiposter;

import io.zhx.asciiposter.board.BoardContentsProvider;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

/**
 * A board that can be stored in and loaded from a file.
 */
public class PersistentBoard extends BoardImpl {
    private static final Logger logger = Logger.getLogger(PersistentBoard
            .class.getName());

    public PersistentBoard(BoardContentsProvider contentsProvider) {
        super(contentsProvider);
    }

    /**
     * Stream out the board's contents.
     *
     * @param outputStream the stream the board is output to.
     * @throws IOException on OutputStream failure
     */
    public void store(OutputStream outputStream)
            throws IOException {
        logger.info("Storing board...");

        int size = getSize();
        for (int i = 0; i < size; i++) {
            outputStream.write(contents[i]);
            outputStream.write("\n".getBytes());
        }
        outputStream.flush();

        logger.info("Board stored");
    }
}
