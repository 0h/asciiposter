package io.zhx.asciiposter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

/**
 * A poster that can output its contents as a PDF file.
 * <p>
 * `float`s are used instead of doubles to avoid constant conversions to suit
 * PDFBox's idiosyncrasies.
 * <p>
 * All members holding a value in millimeters are suffixed with `MM`, and
 * should be converted to points as soon as possible.
 */
public class PDFPoster extends Poster {
    private static final Logger logger = Logger.getLogger(
            PDFPoster.class.getName()
    );

    /**
     * Factor to convert millimeters to points.
     * 1 / (1/72" * 25.4)
     */
    private static final float MM_TO_POINTS_FACTOR = 2.83464566929116F;
    private static final PDFont LETTER_FONT = PDType1Font.COURIER;

    static {
        // Due to the change of the Java color management module towards
        // “LittleCMS”, users can experience slow performance in color
        // operations. Solution: disable LittleCMS in favour of the old KCMS
        // (Kodak Color Management System).
        // @see https://pdfbox.apache.org/2.0/getting-started.html
        System.setProperty(
                "sun.java2d.cmm",
                "sun.java2d.cmm.kcms.KcmsServiceProvider"
        );
    }

    private final PDPage page;
    private final PDRectangle boardArea;
    private final float sizeFactor;
    private final float boardLetterFontSize;

    public PDFPoster(Board board, Size size, double margin) {
        super(board, size, margin);

        // Adjust the poster's measures to PDF units.
        final float pdfWidth = mmToPoints(size.getWidth());
        final float pdfHeight = mmToPoints(size.getHeight());
        final float pdfMargin = mmToPoints(getMargin());
        // Set up the PDF page.
        final float pictureWidth = pdfWidth - (pdfMargin * 2);
        final float pictureHeight = pdfHeight - (pdfMargin * 2);
        page = new PDPage();
        page.setMediaBox(new PDRectangle(pdfWidth, pdfHeight));

        // The picture must be a square, so the smallest of both width and
        // height that fits the page is taken.
        final float pictureSize = Math.min(pictureHeight, pictureWidth);

        // Set up the area where the board will be drawn.
        PDRectangle mediaBox = page.getMediaBox();
        boardArea = new PDRectangle(
                // Where the picture starts horizontally
                (mediaBox.getWidth() / 2) - (pictureSize / 2F),
                // Where the picture starts vertically
                (mediaBox.getHeight() / 2) - (pictureSize / 2F),
                pictureSize, pictureSize
        );

        // How many millimeters per row or column of the board.
        sizeFactor = pictureSize / getBoard().getSize();

        // Calculate other board properties.
        boardLetterFontSize = sizeFactor * 1.2F;
    }

    /**
     * Stream out the contents of the poster.
     *
     * @param outputStream the stream where the PDF file is sent.
     */
    public void store(OutputStream outputStream)
            throws IOException {
        try (final PDDocument document = new PDDocument()) {
            logger.info("Storing PDF...");
            // Add the page to the document.
            document.addPage(page);

            try (final PDPageContentStream contentStream = new
                    PDPageContentStream(document, page, PDPageContentStream
                    .AppendMode.OVERWRITE, true, true)) {

                renderPoster(contentStream);
            }

            // Save the document to a file.
            document.save(outputStream);

            logger.info("PDF stream is ready");
        }
    }

    protected void renderPoster(PDPageContentStream contentStream)
            throws IOException {
        // Render the board to the content stream.
        renderBoard(contentStream);
    }

    /**
     * Render the board.
     *
     * @param contentStream the PDPageContentStream object the board will be
     *                      rendered to.
     * @throws IOException on PDPageContentStream failure
     */
    protected void renderBoard(PDPageContentStream contentStream)
            throws IOException {
        final int boardSize = getBoard().getSize();
        final float pictureSize = getBoardArea().getHeight();
        final float xOrigin = getBoardArea().getLowerLeftX();
        final float yOrigin = getBoardArea().getLowerLeftY();

        logger.info(String.format("Letter font size is %f",
                boardLetterFontSize));

        // The standard ASCII poster features same-sized letters.
        contentStream.setFont(LETTER_FONT, boardLetterFontSize);

        // Y-origin is set to the top-left. Since the PDF's y-axis
        // starts at the bottom left, the y position needs to be
        // subtracted.
        float invertedYOrigin = yOrigin + pictureSize - sizeFactor;

        for (int y = 0; y < boardSize; y++) {

            // Fill up a row.
            for (int x = 0; x < boardSize; x++) {
                byte letter = getBoard().getItem(x, y);

                // Set up the letter's appearance.
                setUpLetter(x, y, contentStream);

                // Position the letter and add it to the content stream.
                contentStream.beginText();
                float xPos = xOrigin + (x * sizeFactor);
                float yPos = invertedYOrigin - (y * sizeFactor);
                contentStream.newLineAtOffset(xPos, yPos);
                contentStream.showText(Character.toString((char) letter));
                contentStream.endText();
            }
        }
    }

    protected void setUpLetter(int boardColumn, int boardRow,
                               PDPageContentStream contentStream)
            throws IOException {
        // Each letter gets its colour from a pixel in the image.
        contentStream.setNonStrokingColor(getLetterColour(boardColumn,
                boardRow));
    }

    /**
     * @param imageX the X position in the image
     * @param imageY the Y position in the image
     * @return the colour in the X,Y coordinates of the image.
     */
    protected Color getLetterColour(int imageX, int imageY) {
        int color = getImage().getRGB(imageX, imageY);

        int red = (color >>> 16) & 0xFF;
        int green = (color >>> 8) & 0xFF;
        int blue = color & 0xFF;

        return new Color(red, green, blue);
    }

    /**
     * Convert millimeters to points, which is PDFBox's unit.
     *
     * @param mm millimeters
     * @return points
     */
    protected static float mmToPoints(double mm) {
        return (float) mm * MM_TO_POINTS_FACTOR;
    }

    public PDRectangle getBoardArea() {
        return boardArea;
    }

    public float getBoardLetterFontSize() {
        return boardLetterFontSize;
    }
}
