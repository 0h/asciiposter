package io.zhx.asciiposter;

/**
 * A rectangular object's measures.
 */
public interface Size {
    float getWidth();

    float getHeight();
}
