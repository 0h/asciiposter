package io.zhx.asciiposter.board;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

/**
 * Fill up a board with an existing byte array.
 */
public class StreamBoardContentsProvider implements BoardContentsProvider {
    private static final Logger logger = Logger.getLogger(
            StreamBoardContentsProvider.class.getName()
    );
    private static final String E_WRONG_SIZE_LINE =
            "Line %d is %d characters long. The board's size is %d";

    private final InputStream stream;

    /**
     * @param stream the stream containing the board's byte array.
     */
    public StreamBoardContentsProvider(InputStream stream) {
        this.stream = stream;
    }

    @Override
    public byte[][] getContents() {
        // Fill up the board.
        try (BufferedReader reader = new BufferedReader(new InputStreamReader
                (stream))) {
            // Read the first line to get the board size.
            String line = reader.readLine();
            int size = line.length();

            logger.info(String.format("Loading order-%d board...", size));

            // Create the board based on the length of the first line.
            byte[][] contents = new byte[size][size];

            int lineIndex = 0;
            int y = 0;
            while (null != line) {
                // Lay the line on the board.
                byte[] lineBytes = line.getBytes();
                for (int x = 0; x < size; x++) {
                    contents[x][y] = lineBytes[x];
                }
//                contents[lineIndex] = line.getBytes();
                // Get the next line.
                line = reader.readLine();
                lineIndex++;
                // Check that the line length matches the board's size.
                if ((null != line) && (line.length() != size)) {
                    throw new RuntimeException(String.format(
                            E_WRONG_SIZE_LINE,
                            lineIndex,
                            line.length(),
                            size
                    ));
                }
                // Increase the row.
                y++;
            }

            logger.info("Board contents ready");

            return contents;
        } catch (IOException exception) {
            // If the contents cannot be loaded, the board is useless,
            // therefore let the exception bubble up.
            throw new RuntimeException(exception);
        }
    }
}
