package io.zhx.asciiposter.board;

/**
 * A means to fill up a board's cells.
 */
public interface BoardContentsProvider {

    /**
     * Provide a two-dimensional byte array to fill up a board.
     */
    byte[][] getContents();
}
