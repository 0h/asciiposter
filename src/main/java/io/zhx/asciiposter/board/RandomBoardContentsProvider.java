package io.zhx.asciiposter.board;

import java.util.Random;

/**
 * Fill up a board with random alphabet characters.
 */
public class RandomBoardContentsProvider implements BoardContentsProvider {
    private final byte[] candidates;
    private final int size;

    /**
     * @param candidates a byte array that random elements will be picked
     *                   from. Use with <code>"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
     *                   .getBytes()</code> to fill up a board with random
     *                   letters from the alphabet.
     * @param size       the size of the board.
     */
    public RandomBoardContentsProvider(byte[] candidates, int size) {
        this.size = size;
        this.candidates = candidates;
    }

    @Override
    public byte[][] getContents() {
        byte[][] contents = new byte[size][size];

        Random random = new Random();

        // Fill up the board.
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                contents[x][y] = candidates[random.nextInt(candidates.length)];
            }
        }

        return contents;
    }

    public int getSize() { return size; }
}
