package io.zhx.asciiposter;

/**
 * A rectangle.
 */
public class Rectangle implements Size {
    private final float width;
    private final float height;

    public Rectangle(float width, float height) {
        if ((width <= 0) || (height <= 0)) {
            throw new IllegalArgumentException("Invalid dimension");
        }
        this.width = width;
        this.height = height;
    }

    @Override
    public float getWidth() { return width;}

    @Override
    public float getHeight() {
        return height;
    }
}
