package io.zhx.asciiposter;

import io.zhx.asciiposter.board.RandomBoardContentsProvider;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;

import static org.junit.Assert.assertEquals;

public class PosterTest {

    private Board testBoard;

    @Before
    public void setUp() {
        testBoard = new BoardImpl(new RandomBoardContentsProvider(
                "ABCDEF".getBytes(),
                10
        ));
    }

    @Test(expected = NullPointerException.class)
    public void testNulls() {
        new Poster(null, null, 0);
    }

    @Test
    public void setGetImage()
            throws Exception {
        Poster poster = new Poster(
                testBoard,
                new Rectangle(2, 2),
                1F
        );

        BufferedImage image = new BufferedImage(1, 1, BufferedImage
                .TYPE_INT_ARGB);

        poster.setImage(image);

        assertEquals(image, poster.getImage());
    }

    @Test
    public void testDimensions() {
        float margin = 1F;
        float width = 2;
        float height = 2;
        Poster poster = new Poster(
                testBoard,
                new Rectangle(width, height),
                margin
        );

        assertEquals(margin, poster.getMargin(), 0);
        assertEquals(width, poster.getSize().getHeight(), 0);
        assertEquals(height, poster.getSize().getWidth(), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void margin_overflows_height() {
        float margin = 2F;
        float width = 3;
        float height = 1;
        new Poster(
                testBoard,
                new Rectangle(width, height),
                margin
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void margin_overflows_width() {
        float margin = 2F;
        float width = 1;
        float height = 3;
        new Poster(
                testBoard,
                new Rectangle(width, height),
                margin
        );
    }
}