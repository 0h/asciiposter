package io.zhx.asciiposter;

import io.zhx.asciiposter.board.RandomBoardContentsProvider;
import org.junit.Test;

import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class PersistentBoardTest {

    @Test
    public void store()
            throws Exception {
        // Create a random board with 1s and 0s.
        int size = 5;
        byte[] candidates = "01".getBytes();
        PersistentBoard board = new PersistentBoard(
                new RandomBoardContentsProvider(candidates, size)
        );

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            // Store it.
            board.store(outputStream);

            // Check that the output matches the expected size and contents.
            byte[] contents = outputStream.toByteArray();
            int length = contents.length;

            // The expected size is `size^2`, plus `size` bytes for the LF at
            // the end of each line of the matrix.
            assertEquals(size * (size + 1), length);

            // Check the contents.
            String candidateString = new String(candidates) + "\n";
            for (byte content : contents) {
                assertFalse(candidateString.indexOf(content) == -1);
            }
        }
    }

}