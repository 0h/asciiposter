package io.zhx.asciiposter;

import io.zhx.asciiposter.board.BoardContentsProvider;
import io.zhx.asciiposter.board.RandomBoardContentsProvider;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class PDFPosterTest {
    private final Size size = new Rectangle(2, 2);
    private final byte[] candidates = "01".getBytes();
    private PDFPoster poster;

    @Before
    public void setUp()
            throws Exception {
        int boardSize = 2;
        Board board = new BoardImpl(
                new RandomBoardContentsProvider(candidates, boardSize)
        );
        int margin = 1;
        poster = new PDFPoster(board, size, margin);
        poster.setImage(new BufferedImage(boardSize, boardSize, BufferedImage
                .TYPE_INT_ARGB));
    }

    @Test
    public void store()
            throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        poster.store(outputStream);

        // Try to load the PDF file.
        PDDocument pdfDocument = PDDocument.load(outputStream.toByteArray());
        // Check that it has just one page.
        assertEquals(1, pdfDocument.getNumberOfPages());
    }

    /**
     * Render the image `dna.jpeg` as a random 100x100 matrix of the letters
     * G, A, T and C, on a DIN A4 PDF file.
     *
     * @throws IOException
     */
    public void testPoster()
            throws IOException {
        // Get the image.
        BufferedImage image = ImageIO.read(
                ClassLoader.getSystemClassLoader()
                           .getResourceAsStream("dna.jpeg")
        );
        // Prepare a matrix filled with a random distribution of the letters
        // G, A, T and C.
        BoardContentsProvider provider = new RandomBoardContentsProvider(
                "GACT".getBytes(),
                image.getWidth()
        );
        // Create a board with the previously generated contents.
        Board board = new BoardImpl(provider);
        // Set up a DIN A4-sized PDF poster, with the GATC board.
        Size dinA4 = new Rectangle(211, 297);
        PDFPoster poster = new PDFPoster(board, dinA4, 10);
        // Set an image to colour up the matrix.
        poster.setImage(image);

        // Render the poster to a file.
        File posterFile = new File("/tmp/testposter.pdf");
        posterFile.createNewFile();
        poster.store(new FileOutputStream(posterFile, false));
    }
}