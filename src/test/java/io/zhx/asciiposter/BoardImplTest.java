package io.zhx.asciiposter;

import io.zhx.asciiposter.board.BoardContentsProvider;
import io.zhx.asciiposter.board.RandomBoardContentsProvider;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoardImplTest {
    @Test
    public void contents_are_accessible_from_descendant_classes() {
        byte[] candidates = {1, 2, 3};
        // Create a 1x1 board content provider with a random value.
        BoardContentsProvider bcp = new RandomBoardContentsProvider(
                candidates,
                1
        );
        // Create a descendant of BoardImpl that accesses the contents member,
        // and get a piece of the content.
        byte justOneCell = new BoardImpl(bcp) {
            public byte getContents() {
                // Make sure contents are different than 0.
                assert(contents[0][0] != 0);
                // Overwrite the contents.
                contents[0][0] = 0;
                // Return the overwritten contents.
                return contents[0][0];
            }
        }.getContents();

        // Ensure the content is the expected.
        assertEquals(0, justOneCell);

    }
}