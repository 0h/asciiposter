package io.zhx.asciiposter;

import org.junit.Test;

public class RectangleTest {
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor() {
        new Rectangle(0, 0);
    }

}