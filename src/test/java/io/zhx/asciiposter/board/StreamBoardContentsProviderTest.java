package io.zhx.asciiposter.board;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class StreamBoardContentsProviderTest {

    @Test
    public void getContents()
            throws Exception {
        String boardSource = "abc\ndef\nghi\n";
        int boardSize = 3;

        InputStream inputStream = new ByteArrayInputStream(
                boardSource.getBytes()
        );
        StreamBoardContentsProvider provider = new StreamBoardContentsProvider(
                inputStream
        );

        // Generate the board contents.
        byte[][] contents = provider.getContents();

        // Make the stream available again.
        inputStream.reset();

        // Test that the matrix matches the stream.
        byte current;
        for (int y = 0; y < boardSize; y++) {
            for (int x = 0; x < boardSize; x++) {
                // Get a byte, skipping new lines.
                do {
                    current = (byte) inputStream.read();
                } while (current == '\n');
                // Test the byte in question.
                assertEquals(contents[x][y], current);
            }
        }
    }


    @Test(expected = RuntimeException.class)
    public void testBrokenSource()
            throws Exception {
        // The last line is one character short.
        String boardSource = "abc\ndef\ngh\n";
        int boardSize = 3;

        InputStream inputStream = new ByteArrayInputStream(
                boardSource.getBytes()
        );
        StreamBoardContentsProvider provider = new StreamBoardContentsProvider(
                inputStream
        );

        // Generate the board contents.
        provider.getContents();
    }
}