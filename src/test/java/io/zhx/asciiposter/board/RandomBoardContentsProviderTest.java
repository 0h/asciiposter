package io.zhx.asciiposter.board;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class RandomBoardContentsProviderTest {
    private final int size = 5;
    private final byte[] candidates = "01".getBytes();
    private RandomBoardContentsProvider provider;

    @Before
    public void setUp() {
        provider = new RandomBoardContentsProvider(candidates, size);
    }

    @Test
    public void testSizeAndContents()
            throws IOException {
        // Check that the output matches the expected size and contents.
        byte[][] contents = provider.getContents();

        assertEquals(size, provider.getSize());

        // Check the contents.
        String candidateString = new String(candidates);
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                assertFalse(candidateString.indexOf(contents[x][y]) == -1);
            }
        }
    }
}